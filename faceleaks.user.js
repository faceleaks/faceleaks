// ==UserScript==
// @name           FaceLeaks
// @version        1.0.1
// @namespace      http://www.faceleaks.info
// @description    Attaches a leak button to Facebook photos, allowing to leak them to www.faceleaks.info website
// @license        No license
// @author         Society of Algorithm

// @include        http://www.facebook.com/*
// @match          http://www.facebook.com/*
// @include        https://www.facebook.com/*
// @match          https://www.facebook.com/*

// @include        http://localhost/faceleaks/*
// @match          http://localhost/faceleaks/*
// @include        https://localhost/faceleaks/*
// @match          https://localhost/faceleaks/*

// @exclude        http://*.facebook.com/sharer*
// @exclude        http://*.facebook.com/ajax/*
// @exclude        http://*.facebook.com/plugins/*

// @exclude        http://apps.facebook.com/*
// @exclude        http://*facebook.com/apps/*

// ==/UserScript==

(function(d){

// Inserts javascript that will be called by the leakButton ---- na koniec hlavicky --!!!!!teda je potrebny element <head> v stranke
// pop up
var scriptElement2 = d.createElement('script');
scriptElement2.type = 'text/javascript';
scriptElement2.innerHTML =  'function leakItPopUp(theURL) {    mx = Math.round((screen.width-410)/2);   my = Math.round((screen.height-210)/2);   var myWin2=window.open(theURL,"leakbook","status=yes,toolbar=no,directories=no,scrollbars=yes,location=no,resizable=yes,menubar=no,left="+mx+",top="+my+",width=400,height=200") . focus(); }';
d.getElementsByTagName("head")[0].appendChild(scriptElement2);

function safeUrlEncode(s)
{
        s=escape(s);
        s=s.replace(/\+/g, "%2B"); //replace '+'
        s=s.replace(/\//g, "%2F"); //replace '/'
        s=s.replace("'", "%27"); //replace ''' - z nejakeho dovodu tento zameni vsetky apostrofy, na rozdiel od predch dvoch ked boli v uvodzovkach..
        s=s.replace('"', "%22"); //replace '"' 
	return s;
}

d.getElementsByClassName = function(cl) {
var retnode = [];
var myclass = new RegExp('\\b'+cl+'\\b');
var elem = this.getElementsByTagName('*');
for (var i = 0; i < elem.length; i++) {
  var classes = elem[i].className;
  if (myclass.test(classes)) retnode.push(elem[i]);
}
return retnode;
}; 


function addButton () {
        var err;


// Get the location on the page where you want to create the button
        var targetDiv, targetDivChild;
   // black pop-up layer
        try { 
          targetDiv = d.getElementById("fbPhotoTheater").getElementsByClassName("UIActionLinks UIActionLinks_bottom")[0];
          targetDivChild = d.getElementById("fbPhotoTheater").getElementsByClassName("UIActionLinks UIActionLinks_bottom")[0].getElementsByTagName("label")[0];
        } 
        catch (err) { try { //ak predosle vyhodi error, tak:
   // biele pozadie
          targetDiv = d.getElementsByClassName("UIStandardFrame_Container")[0].getElementsByClassName("UIActionLinks UIActionLinks_bottom")[0];
          targetDivChild = d.getElementsByClassName("UIStandardFrame_Container")[0].getElementsByClassName("UIActionLinks UIActionLinks_bottom")[0].getElementsByTagName("label")[0];
        } catch (err) { return;
        } } 
        err = null;



      if (targetDiv.className.indexOf("leakLink") < 0) {  // cyklus chodi kvoli force refresh domu stale dokola, takze po pridani leak buttonu pridaj class do spanu aby nabuduce uz button nepridaval

        targetDiv.className += "leakLink"; // pridaj novu class ~ stamp ze leak button uz bol pridany, pretoze funkcia sa vola kazdych 125 ms, aby leak nezmizol po fb refreshoch pripadnych v ramci stranky


//Get Direct Photo Link
        var photoLink, photoLinkTemp;
//Photo @ ALBUM
        try { //ak predosle vyhodi error, tak:
//  normal-res z albumu:
          photoLink = d.getElementById("myphotolink").getElementsByTagName("img")[0].src;
//Photo @ POP-UP BLACK
        } 
        catch (err) { try { //ak predosle vyhodi error, tak:
//  hi-res z pop-up black, 'photo download'
          photoLink = d.getElementById("fbPhotosTheaterActions").getElementsByTagName("a")[2].href;
          if (photoLink.indexOf("?dl=1") > 0) { //lebo ked autor fotky neni friend (tvoj friend je tam len tagnuty), tak tam neni ?dl=1
            photoLink = photoLink.substring(0,photoLink.length-5); //lebo na konci je ?dl=1
          }
        } 
        catch (err) { try { //ak predosle vyhodi error, tak:
//  normal-res z pop-up black: <table class="uiGrid fbPhotosGrid"><td class="vTop hLeft"><div class="dragWrapper"> potom sipka img back <div class="tagWrapper"><i style=XXX>
          photoLinkTemp = d.getElementsByClassName("fbPhotosGrid")[0].getElementsByTagName("i")[0].style.backgroundImage;
          photoLink = photoLinkTemp.substring(5,photoLinkTemp.length-2); //lebo je v url("xx.jpg")
        } catch (err) {
        } } } 
        err = null;


//Get photo author
        var photoAuthor;
        try {
    // photo author @ ALBUM
	  photoAuthor = d.getElementById("photoinalbum").getElementsByTagName("a")[1].innerHTML; 
        } 
        catch (err) { try { //ak predosle vyhodi error, tak:
    // photo author @ pop-up black 
	  photoAuthor = d.getElementById("fbPhotoTheaterContributors").getElementsByTagName("a")[0].innerHTML; // meno autora fotky
        } catch (err) {
        } }
        err = null;


//Get photo title (album / profile photo)
        var pageDesc;
        try {
    // black pop-up tagged (autor neni friend)
	  pageDesc = d.getElementById("fbPhotoTheaterTitle").getElementsByTagName("a")[1].innerHTML; 
        } 
        catch (err) { try { //ak predosle vyhodi error, tak:
    // black pop-up friend authored -- musi ist za predosle, lebo tu je viac a href v danom id
	  pageDesc = d.getElementById("fbPhotoTheaterTitle").getElementsByTagName("a")[0].innerHTML; 
        } 
        catch (err) { try { //ak predosle vyhodi error, tak:
    // nazov albumu @ ALBUM -- white tagged (autor neni friend)  & friend authored
	  pageDesc = d.getElementById("photoinalbum").getElementsByTagName("a")[0].innerHTML; 
        } 
        catch (err) { try { //ak predosle vyhodi error, tak:
    // inak z title 
	  pageDesc = d.getElementsByTagName("title")[0].innerHTML; 
        } catch (err) { // alert(err.message); 
        } } } }
        err = null;


        // Get http page link
        var pageLink = d.location.href;


        photoLink=safeUrlEncode(photoLink);
        photoAuthor=safeUrlEncode(photoAuthor);
        pageDesc=safeUrlEncode(pageDesc);
        pageLink=safeUrlEncode(pageLink);


	// Create the button and set its attributes
        var middot = d.createTextNode(" · ");
	var leakButton = d.createElement('label');
	leakButton.innerHTML = 'Leak';
	leakButton.className = 'comment_link';
	leakButton.setAttribute("onclick", "leakItPopUp('http://www.faceleaks.info/leakit.php?f="+photoLink+"&a="+photoAuthor+"&t="+pageDesc+"&l="+pageLink+"');");

    // black pop-up friend authored & tagged
	if (!targetDivChild) { targetDiv.insertBefore(middot,targetDivChild); }  //pretoze if not friend authored tak tam neni ziadny iny button a treba 'leak' button oddelit od datumu middotom 
        targetDiv.insertBefore(leakButton,targetDivChild); 
	if (targetDivChild) { targetDiv.insertBefore(middot,targetDivChild); }   //pretoze if not friend authored tak za nim neni ziadny iny button 

        delete photoLink,photoLinkTemp,photoAuthor,pageDesc,middot,leakButton;

      }

  delete err,targetDiv,targetDivChild;
  return false;
}



    /* Start Script */
    if (content = d.getElementsByTagName('body')[0]) {
        addButton();
//        var t;
//        content.addEventListener('DOMNodeInserted', function() { clearTimeout(t); t = setTimeout(addButton, 125); }, false);
//!!! z nejakeho dovodu ked mam instalnuty HTTPS Everywhere plugin, tak black layer popup sa ^ tymto refreshne do bieleho
    }

})(document);



